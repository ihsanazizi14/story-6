from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from .models import *
from .forms import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class Lab6UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, status_form)

    def test_model_can_create_object(self):
        new_status = Status.objects.create(status="status test complete")
        count = Status.objects.all().count()
        self.assertEqual(count, 1)

    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )
      
    def test_form_validation_for_correct_items(self):
        form = StatusForm(data={'status':'testtest123'})
        self.assertTrue(form.is_valid())

    def test_post_success_and_render_the_result(self):
        test = 'testtest123'
        response_post = self.client.post('/', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response= self.client.get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test , html_response)

    def test_post_error_and_render_the_result(self):
        test = 'testtest123'
        response_post = self.client.post('/', {'status': ''})
        self.assertEqual(response_post.status_code, 200)

        response= self.client.get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test , html_response)

class Lab6FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab6FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()
    
    # Test selalu error dan belum bisa di solve
    # def test_form_input(self):
    #     selenium = self.selenium
    #     selenium.get('http://127.0.0.1:8000/')

    #     assert 'testtest123' not in selenium.page_source

    #     status = selenium.find_element_by_id('id_status')
    #     submit = selenium.find_element_by_id('submit')

    #     status.send_keys('testtest123')

    #     submit.send_keys(Keys.RETURN)

    #     selenium.get('http://127.0.0.1:8000/')
        # assert 'testtest123' in selenium.page_source

    def test_element_in_project(self):
        self.selenium.get(self.live_server_url)


        alert = self.selenium.find_element_by_class_name("classForm")
        self.assertEquals(

            alert.find_element_by_tag_name('h1').text,
            'Halo, apa kabar?'

        )
        time.sleep(10)

    def test_response(self):
        project1 = Status.objects.create(

            status = "Saya Baik"

        )
        self.selenium.get(self.live_server_url)
        alert = self.selenium.find_element_by_class_name("kotak")

        self.assertEquals(

            alert.find_element_by_tag_name('p').text,
            'Saya Baik'
        )

