from django.urls import path
from . import views

app_name = 'my_app'

urlpatterns = [
    path('', views.status_form, name='status_form'),

]