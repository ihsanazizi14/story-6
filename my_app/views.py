from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

def status_form(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('my_app:status_form')

    else:
        form = StatusForm()
    return render(request, 'landing.html', {'form': form, 'list_status': Status.objects.all()})

